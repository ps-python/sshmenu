#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export PATH

#First update pip3
echo "Update pip" 
python3 -m pip install --upgrade pip

# Need virtualenv
echo  "Installing virtualenv"
pip3 install virtualenv

echo ""
echo "# Step 1 : installing Python3 stuff"
echo ""

# Install all stuff in /usr/share/sshmenu-p3
cd /usr/share/
echo "Installing all packages into \"/usr/share/sshmenu-p3\" "
virtualenv -p /usr/bin/python3 sshmenu-p3/
source sshmenu-p3/bin/activate
pip3 install pygobject


exit 0






