#!/usr/bin/python
# -*- coding: utf-8 -*-
#

#  +-- ssh menu   ----------------------------------------------------------------+
#  |                                                                              |
#  |            _                                                                 |          |
#  |           | |                                                                | 
#  |  ___  ___ | |___    _ __ ___   ___  _ __  _   _                              |
#  | / __|/ __||  _  \  | '_ ` _ \ / _ \| '_ \| | | |                             |
#  | \__ \\__ \| | | |  | | | | | |  __/| | | | |_| |                             | 
#  | |___/|___/|_| |_|  |_| |_| |_|\___||_| |_|_____/                             | 
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# *                                                                                         *
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * sshmenu is free software;  you can redistribute it and/or modify it under the           *
# *  terms of the  license mentionned above.                                                *
# * sshmenu is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY     * 
# * sshmenu is a fork of SSHplus by Anil Gulecha                                            *                          
# *                                                                                         *
# * ----------------------------------------------------------------------------------------*



""" Python 3. """
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

gi.require_version('AppIndicator3', '0.1')
from gi.repository import AppIndicator3

gi.require_version('Notify', '0.7')
from gi.repository import Notify


""" Common libs """
import getopt
import os
import re
import sys
import shlex

_TITLE = "SSH MENU"

_VERSION = "1.0"

_CONFIG_FILE = os.getenv("HOME") + "/.sshmenu"

_ABOUT_TXT = """A simple application starter as appindicator.

To add items to the menu, edit the file <i>.sshmenu</i> in your home directory. Each entry must be on a new line in this format:

<tt>NAME|COMMAND|ARGS</tt>

If the item is clicked in the menu, COMMAND with arguments ARGS will be executed. ARGS can be empty. To insert a separator, add a line which only contains "sep". Lines starting with "#" will be ignored. You can set an unclickable label with the prefix "label:". Items from sshmenu configuration will be automatically added (except nested items). To insert a nested menu, use the prefix "folder:menu name". Subsequent items will be inserted in this menu, until a line containing an empty folder name is found: "folder:". After that, subsequent items get inserted in the parent menu. That means that more than one level of nested menus can be created.

Example file:
<tt><small>
Show top|gnome-terminal|-x top
sep

# this is a comment
label:SSH connections
# create a folder named "Home"
folder:Home
SSH Ex|gnome-terminal|-x ssh user@1.2.3.4
# to mark the end of items inside "Home", specify and empty folder:
folder:
# this item appears in the main menu
SSH Ex|gnome-terminal|-x ssh user@1.2.3.4

label:RDP connections
RDP Ex|rdesktop|-T "RDP-Server" -r sound:local 1.2.3.4

</small></tt>
"""



"""
getopt
"""
options, remainder = getopt.getopt(sys.argv[1:], 'c:t:', 
                                   ['configFile=',
                                    'title='
                                    ])


if len(options) > 0: 
    for opt, arg in options:
        if opt in ('-c', '--configFile'):
            if len(arg) > 0:
                _CONFIG_FILE = arg

        if opt in ('-t', '--title'):
            if len(arg) > 0:
                _TITLE = arg


def menuitem_response(w, item):
    if item == '_about':
        show_help_dlg(_ABOUT_TXT)
    elif item == '_refresh':
        newmenu = build_menu()
        ind.set_menu(newmenu)
        Notify.init(APPINDICATOR_ID)
        Notify.Notification(summary="SSHMENU refreshed", body="Menu list was refreshed from %s" % _CONFIG_FILE).show()
    elif item == '_quit':
        sys.exit(0)
    elif item == 'folder':
        pass
    else:
        print(item)
        os.spawnvp(os.P_NOWAIT, item['cmd'], [item['cmd']] + item['args'])
        os.wait3(os.WNOHANG)



def show_help_dlg(msg, error=False):
    if error:
        dlg_icon = Gtk.MessageType.ERROR
    else:
        dlg_icon = Gtk.MessageType.INFO
    md = Gtk.MessageDialog(None, 0, dlg_icon, Gtk.ButtonsType.OK)
    try:
        md.set_markup("<b>Neo sshmenu (from SSHplus) %s</b>" % _VERSION)
        md.format_secondary_markup(msg)
        md.run()
    finally:
        md.destroy()
    


def add_separator(menu):
    separator = Gtk.SeparatorMenuItem()
    separator.show()
    menu.append(separator)



def add_menu_item(menu, caption, item=None):
    menu_item = Gtk.MenuItem(caption)
    if item:
        menu_item.connect("activate", menuitem_response, item)
    else:
        menu_item.set_sensitive(False)
    menu_item.show()
    menu.append(menu_item)
    return menu_item



def get_sshmenuconfig():
    if not os.path.exists(_CONFIG_FILE):
        return []

    app_list = []
    f = open(_CONFIG_FILE, "r")
    try:
        for line in f.readlines():
            line = line.rstrip()
            if not line or line.startswith('#'):
                continue
            elif line == "sep":
                app_list.append('sep')
            elif line.startswith('label:'):
                app_list.append({
                    'name': 'LABEL',
                    'cmd': line[6:], 
                    'args': ''
                })
            elif line.startswith('folder:'):
                app_list.append({
                    'name': 'FOLDER',
                    'cmd': line[7:], 
                    'args': ''
                })
            else:
                try:
                    name, cmd, args = line.split('|', 2)
                    app_list.append({
                        'name': name,
                        'cmd': cmd,
                        'args': [n.replace("\n", "") for n in shlex.split(args)],
                    })
                except ValueError:
                    print("The following line has errors and will be ignored:\n%s" % line)
    finally:
        f.close()
    return app_list



def build_menu():
    if not os.path.exists(_CONFIG_FILE) :
        show_help_dlg("<b>ERROR: No sshmenu or sshplus file found in home directory</b>\n\n%s" % \
             _ABOUT_TXT, error=True)
        sys.exit(1)


    #Add sshmenu config items if any
    app_list = get_sshmenuconfig()
    if app_list != []:
        app_list = ["sep",{'name': 'LABEL','cmd': "SSHmenu",'args': ''}] + app_list

    menu = Gtk.Menu()
    menus = [menu]

    for app in app_list:
        if app == "sep":
            add_separator(menus[-1])
        elif app['name'] == "FOLDER" and not app['cmd']:
            if len(menus) > 1:
                menus.pop()
        elif app['name'] == "FOLDER":
            menu_item = add_menu_item(menus[-1], app['cmd'], 'folder')
            menus.append(Gtk.Menu())
            menu_item.set_submenu(menus[-1])
        elif app['name'] == "LABEL":
            add_menu_item(menus[-1], app['cmd'], None)
        else:
            add_menu_item(menus[-1], app['name'], app)

    add_separator(menu)
    add_menu_item(menu, 'Refresh', '_refresh')
    add_menu_item(menu, 'About', '_about')
    add_separator(menu)
    add_menu_item(menu, 'Quit', '_quit')
    return menu



if __name__ == "__main__":
    APPINDICATOR_ICON = "/usr/share/sshmenu/icons/sshmenu.png"
    APPINDICATOR_ID   =  "Neo sshMenu"
    ind = AppIndicator3.Indicator.new(APPINDICATOR_ID, 
                                    APPINDICATOR_ICON,
                                    AppIndicator3.IndicatorCategory.APPLICATION_STATUS)
    ind.set_label(_TITLE,"label")
    ind.set_status(AppIndicator3.IndicatorStatus.ACTIVE)

    appmenu = build_menu()
    ind.set_menu(appmenu)
    Notify.init(APPINDICATOR_ID)
    Gtk.main()

